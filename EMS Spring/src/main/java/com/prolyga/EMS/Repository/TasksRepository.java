package com.prolyga.EMS.Repository;

import com.prolyga.EMS.Model.Tasks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TasksRepository extends JpaRepository<Tasks, Long> {

    List<Tasks> getTasksListByUserId(Long userId);

    Optional<Tasks> getByTaskId(Long taskId);
}

package com.prolyga.EMS.Repository;

import com.prolyga.EMS.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAllByRole(String role);

    List<User> findAllByDepartment(String department);

}

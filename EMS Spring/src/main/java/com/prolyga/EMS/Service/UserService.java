package com.prolyga.EMS.Service;

import com.prolyga.EMS.Model.User;
import com.prolyga.EMS.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> getUsersList() {
        return userRepository.findAll();
    }

    public List<User> getUsersByRole(String role) {
        return userRepository.findAllByRole(role);
    }

    public List<User> getUserByDepartment(String department){
        return userRepository.findAllByDepartment(department);
    }

    public Optional<User> getUserById(long id){
        return userRepository.findById(id);
    }

    public User addUser(User user) {
        return userRepository.save(user);
    }
    public void deleteUserById(long userId) throws Exception {
        getUserById(userId).ifPresent(userRepository::delete);
    }
}

package com.prolyga.EMS.Service;

import com.prolyga.EMS.Model.Tasks;
import com.prolyga.EMS.Repository.TasksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TasksService {

    @Autowired
    private TasksRepository tasksRepository;

    public List<Tasks> getTasksList() {
        return tasksRepository.findAll();
    }

    public List<Tasks> getTasksListByUserId(long id) {
        return tasksRepository.getTasksListByUserId(id);
    }

    public Tasks addTask(Tasks task) {
        return tasksRepository.save(task);
    }

    public Tasks getTaskByTaskId(long taskid) throws Exception {
        return tasksRepository.getByTaskId(taskid).orElseThrow(() -> new Exception("Task not found for this id :: " + taskid));
    }

    public ResponseEntity<Tasks> updateTask(long id, Tasks taskDetails) throws Exception {
        Tasks task = tasksRepository.findById(id).orElseThrow(() -> new Exception("Task not found for this id :: " + id));
        task.setDescription(taskDetails.getDescription());
        task.setDeadline(taskDetails.getDeadline());
        final Tasks updatedTask = tasksRepository.save(task);
        return ResponseEntity.ok(updatedTask);
    }

    public void deleteTaskById(long taskId) throws Exception {
        tasksRepository.delete(getTaskByTaskId(taskId));
    }

}

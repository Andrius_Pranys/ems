package com.prolyga.EMS.Controller;

import com.prolyga.EMS.Model.Tasks;
import com.prolyga.EMS.Service.TasksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class TasksController {

    @Autowired
    private TasksService tasksService;

    @GetMapping("/tasks")
    private @ResponseBody
    List<Tasks> getTasksList() {
        return tasksService.getTasksList();
    }

    @GetMapping("/users/usertasks/{id}")
    private @ResponseBody
    Iterable<Tasks> getTasksListByUserId(@PathVariable(value = "id") Long id) {
        return tasksService.getTasksListByUserId(id);
    }


    @PostMapping("/tasks")
    public Tasks addTask(@RequestBody Tasks task) {
        return tasksService.addTask(task);
    }

    @PutMapping("/tasks/{id}")
    public ResponseEntity<Tasks> updateTask(@PathVariable(value = "id") Long id,
                                            @RequestBody Tasks taskDetails) throws Exception {
        Tasks task = tasksService.getTaskByTaskId(id);
        task.setDescription(taskDetails.getDescription());
        task.setDeadline(taskDetails.getDeadline());
        final Tasks updatedTask = tasksService.addTask(task);
        return ResponseEntity.ok(updatedTask);
    }

    @DeleteMapping("tasks/{id}")
    public void deleteTask(@PathVariable(value = "id") Long id) throws Exception {
        tasksService.deleteTaskById(id);
    }

}

package com.prolyga.EMS.Controller;

import com.prolyga.EMS.Exception.UserNotFoundException;
import com.prolyga.EMS.Model.User;
import com.prolyga.EMS.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping("/users")
    private @ResponseBody
    List<User> getUsersList() {
        return userService.getUsersList();
    }


    @GetMapping("/users/{role}")
    List<User> getEmployeesList(@PathVariable(value = "role") String role) {
        return userService.getUsersByRole(role);
    }

    @GetMapping("users/department/{department}")
    List<User> getEmplyeesListByDepartment(@PathVariable(value = "department") String department){
        return userService.getUserByDepartment(department);
    }


    @GetMapping("/user/{id}")
    public User getUserById(@PathVariable(value = "id") Long id) {
        Optional<User> optionalUser = userService.getUserById(id);
        if (!optionalUser.isPresent()) {
            throw new UserNotFoundException(id);
        }
        return optionalUser.get();
    }

    @PostMapping("/users")
    public User addUser(@RequestBody User user) {
        return userService.addUser(user);
    }

    @DeleteMapping("/users/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable(value = "id") Long id) throws Exception {
        userService.deleteUserById(id);

    }
}


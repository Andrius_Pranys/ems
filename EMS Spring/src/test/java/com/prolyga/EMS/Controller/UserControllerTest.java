package com.prolyga.EMS.Controller;

import com.prolyga.EMS.IntegrationTest;
import com.prolyga.EMS.Model.User;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;

public class UserControllerTest extends IntegrationTest {

    private static User TEST_USER = new User(
            1L,
            "Bob",
            "admin",
            "asd@gmail.com",
            "123",
            "Sales",
            "Salesman"
    );

    @Test
    public void get_UserById_returnsUser_200() {

        ValidatableResponse response = getAllUsers();

        response.assertThat().statusCode(HttpStatus.OK.value())
                .body("content.size()", equalTo(0));
    }

    @Test
    public void post_newUser_returnsCreatedUser_201() {
        ValidatableResponse response = createUser(TEST_USER);

        response.assertThat().statusCode(HttpStatus.OK.value())
                .body("name", equalTo("Bob"))
                .body("email", equalTo("asd@gmail.com"))
                .body("role", equalTo("admin"))
                .body("phone", equalTo("123"))
                .body("department", equalTo("Sales"))
                .body("position", equalTo("Salesman"));
    }

    @Test
    public void delete_user_returnsNoContent_204() {
        createUser(TEST_USER)
                .assertThat()
                .statusCode(HttpStatus.OK.value());
        ValidatableResponse usersResponse = getAllUsers();
        usersResponse.assertThat().statusCode(HttpStatus.OK.value()).body("size()", is(1));
        User user = Arrays.asList(usersResponse.extract().as(User[].class)).get(0);
        deleteUser(user.getUserId()).assertThat().statusCode(HttpStatus.NO_CONTENT.value());
        getAllUsers().assertThat().statusCode(HttpStatus.OK.value()).body("size()", is(0));
    }

    @Test
    public void get_UserById_returnsNotFound_404() {
        getUserById(5L).assertThat().statusCode(HttpStatus.NOT_FOUND.value())
                .body("message", is("User with id 5 not found"));
    }

}


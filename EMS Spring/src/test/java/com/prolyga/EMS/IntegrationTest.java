package com.prolyga.EMS;

import com.prolyga.EMS.Model.User;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static io.restassured.RestAssured.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = EmsApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles({"integration"})
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/group2.sql")
public abstract class IntegrationTest {

    @Value("${local.server.port}")
    private int ports;

    public String buildPath(String... args) {
        return String.join("", args);
    }

    @BeforeAll
    public void setUp() {
        port = ports;
        baseURI = "http://localhost";
    }

    public ValidatableResponse getUserById(Long userId) {
        return given().contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE).when().get("/user/" + userId).then();
    }

    public ValidatableResponse getAllUsers() {
        return given().contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE).when().get("/users").then();
    }

    public ValidatableResponse createUser(User user) {
        return given().contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE).body(user).when().post("/users").then();
    }

    public ValidatableResponse deleteUser(Long id) {
        return given().contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE).when().delete("/users/" + id).then();
    }
}
